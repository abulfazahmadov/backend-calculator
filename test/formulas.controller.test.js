const expect = require('chai').expect;
const sinon = require('sinon');
const Controllers = require('../controllers/formulas');

describe('Formulas Controllers test', () => {
    beforeEach(function () {
        res = {
            send: sinon.spy(),
        };
        req = {
            params: {
                number: 1,
            }
        }
    });

    it('Should send sin', () => {
        Controllers.getSin(req, res);
        const x = Math.sin(Number.parseFloat(req.params.number));
        expect(res.send.calledWith({
            result: x
        })).to.be.true;
    });

    it('Should send cos', () => {
        Controllers.getCos(req, res);
        const x = Math.cos(Number.parseFloat(req.params.number));
        expect(res.send.calledWith({
            result: x
        })).to.be.true;
    });

    it('Should send tan', () => {
        Controllers.getTan(req, res);
        const x = Math.tan(Number.parseFloat(req.params.number));
        expect(res.send.calledWith({
            result: x
        })).to.be.true;
    });

    it('Should send ctg', () => {
        Controllers.getCtg(req, res);
        const x = 1/Math.tan(Number.parseFloat(req.params.number));
        expect(res.send.calledWith({
            result: x
        })).to.be.true;
    });
});
