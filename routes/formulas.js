var express = require('express');
var router = express.Router();
var FormulasController = require('../controllers/formulas')

router.get('/sin/:number', FormulasController.getSin);

router.get('/cos/:number', FormulasController.getCos);

router.get('/tan/:number', FormulasController.getTan);

router.get('/ctg/:number', FormulasController.getCtg);

module.exports = router;
