var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var indexRouter = require('./routes/index');
var formulasRouter = require('./routes/formulas');

var app = express();

app.use(cors()); // we assume this is a public formulas API server.
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/formulas', formulasRouter);

module.exports = app;
