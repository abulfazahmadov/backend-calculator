exports.getSin = function(req, res, next) {
    const x = Number.parseFloat(req.params.number);
    res.send({ result: Math.sin(x)});
};

exports.getCos = function(req, res, next) {
    const x = Number.parseFloat(req.params.number);
    res.send({ result: Math.cos(x)});
};

exports.getTan = function(req, res, next) {
    const x = Number.parseFloat(req.params.number);
    res.send({ result: Math.tan(x)});
};

exports.getCtg = function(req, res, next) {
    const x = Number.parseFloat(req.params.number);
    res.send({ result: 1/Math.tan(x)});
};